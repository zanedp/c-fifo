require 'pp' # pretty print

SVN_DEPENDENCIES = {
  'Unity' => 'https://github.com/ThrowTheSwitch/Unity/trunk'
}
SVN_DIRS = SVN_DEPENDENCIES.keys # just the local directories

COMPILER = "gcc"
COMPILER_OPTS = "-Wall -std=c99 -g -I src -I Unity/src -DUNITY_SUPPORT_64"
EXE = 'test_runner.exe'

OBJ_DIR = 'obj/'
RUNNER_DIR = 'test_runners/'
SRC_C_FILES = Rake::FileList['src/*.c']
TEST_C_FILES = Rake::FileList['test/test_*.c']

TEST_RUNNER_HASH = Hash[TEST_C_FILES.map{|r| [RUNNER_DIR + r.sub('.c', '_Runner.c'), r]}]
TEST_RUNNER_C_FILES = TEST_RUNNER_HASH.keys
C_FILES = SRC_C_FILES + TEST_C_FILES + TEST_RUNNER_C_FILES + FileList['Unity/src/*.c']
O_HASH = Hash[C_FILES.map{|f| [(OBJ_DIR + f.sub(/\.c$/, '.o')), f]}]
O_FILES = O_HASH.keys

task :default => :run

desc "Runs the unit tests (default)"
task :run => :build do
  sh "./#{EXE}"
end

directory RUNNER_DIR
directory OBJ_DIR

desc "Compiles #{EXE}"
task :build => EXE

file EXE => O_FILES do |t|
  all_o_files = O_FILES.join ' '
  sh "#{COMPILER} #{COMPILER_OPTS} #{all_o_files} -o #{EXE}"
end

rule '.i' => '.c' do |t|
  sh "#{COMPILER} #{COMPILER_OPTS} #{t.source} -E > #{t.name}"
end

TEST_RUNNER_C_FILES.each do |rc|
  file rc => TEST_RUNNER_HASH[rc] do
    unless File.exist? File.dirname(rc)
      mkdir_p File.dirname(rc)
    end
    sh "ruby Unity/auto/generate_test_runner.rb #{TEST_RUNNER_HASH[rc]} #{rc}"
  end
end

O_FILES.each do |obj|
  file obj => O_HASH[obj] do |t|
    out_dir = File.dirname(obj)
    unless File.exist? out_dir
      mkdir_p out_dir
    end
    sh "#{COMPILER} #{COMPILER_OPTS} -c #{O_HASH[obj]} -o #{obj}"
  end
end



namespace :svn do
  desc "List known SVN dependencies"
  task :list do
    pp SVN_DEPENDENCIES
  end

  # This sets up a bunch of "file" dependencies for each of the directories in the SVN_DEPENDENCIES table
  SVN_DIRS.each do |dir|
    file dir do
      svn_url = SVN_DEPENDENCIES[dir]
      sh "svn co #{svn_url} #{dir}"
    end
  end

  desc "Checks out a dependency from SVN. Use * to checkout all."
  task :co, :path_name do |t, args|
    if args.path_name == '*'
      SVN_DIRS.each do |dir|
        Rake::Task[dir].invoke
      end
    else
      Rake::Task[args.path_name].invoke
    end
  end

  desc "Deletes from disk all SVN dependencies"
  task :rm do
    SVN_DIRS.each do |dir|
      rm_rf dir
    end
  end
end

desc "Use SVN to checkout the necessary dependencies"
task :init_dir => SVN_DIRS + [OBJ_DIR, RUNNER_DIR]

desc "Deletes generated files"
task :clean do
  rm_f EXE
  rm_rf OBJ_DIR
  rm_rf RUNNER_DIR
end
