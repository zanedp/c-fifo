/**
 * @file fifo.h
 *
 * Provides a data-type agnostic circular fifo queue.
 *
 * ## Note
 * This implementation is not re-entrant. If re-entrancy is required then the client
 * code is reponsible for providing that through a mutex, disabling interrupts, etc.
 *
 *
 * ## Example
 * ~~~~{.c}
 * FIFO_t myFIFO;
 * uint8_t fifoBuffer[50];
 * FIFO_Init(&myFIFO, fifoBuffer, sizeof(fifoBuffer));
 * int writeData = 12345;
 * if (FIFO_MaxWriteSize(&myFIFO) >= sizeof(writeData))
 * {
 *    // OK to ignore return value because we verified that we
 *    // would have enough space by testing FIFO_MaxWriteSize()
 *    FIFO_Write(&myFIFO, &writeData, sizeof(writeData));
 * }
 * else
 * {
 *    printf("Not enough room\n");
 * }
 *
 * int readData;
 * if (FIFO_Read(&myFIFO, &readData, sizeof(readData)) == sizeof(readData))
 * {
 *    printf("Read back the value %d\n", readData);
 * }
 * ~~~~
 */
#ifndef FIFO_H
#define FIFO_H

#include <stddef.h>
#include <stdbool.h>

/**
 * Structure representing the state of a FIFO queue.
 * None of the members of this structure are intended to be accessed outside of the
 * FIFO functions.
 *
 * If you need to serialize the state of a FIFO, you can serialize this structure and separately
 * what buf points to, but when you deserialize, you will need to update the buf pointer
 * appropriately.
 */
typedef struct
{
   size_t readCursor; ///< buf index to be read next. Set to invalid index when empty.
   size_t writeCursor; ///< buf index to be written next
   size_t size; ///< size of buf in bytes. Does not need to be a power of two.
   unsigned char *buf; ///< buffer for storing queue items
} FIFO_t;

/**
 * Initialize the FIFO structure with the given buffer and size.
 *
 * @param f fifo to initialize
 * @param buf buffer to be used for storing fifo items tracked by f
 * @param size number of bytes allocated to buf
 */
void FIFO_Init(FIFO_t *f, void *buf, size_t size);

/**
 * Indicates whether specified fifo contains any unread data.
 */
bool FIFO_IsEmpty(const FIFO_t *f);

/**
 * Indicates whether specified fifo can have more data written to it.
 */
bool FIFO_IsFull(const FIFO_t *f);

/**
 * Indicates how many bytes are available to write to a fifo queue.
 */
size_t FIFO_MaxWriteSize(const FIFO_t *f);

/**
 * Indicates how many bytes are available for reading from a fifo queue.
 */
size_t FIFO_MaxReadSize(const FIFO_t *f);

/**
 * Writes a specified number of bytes to a fifo queue.
 *
 * @param f fifo to copy data from
 * @param bufIn source of data
 * @param nbytes number of bytes to read from bufIn, if available.
 * @return number of bytes copied to fifo from bufIn.
 */
size_t FIFO_Write(FIFO_t *f, const void *bufIn, size_t nbytes);

/**
 * Reads a specified number of bytes from a fifo queue.
 *
 * @param f fifo to copy data from
 * @param bufOut destination for data
 * @param nbytes number of bytes to copy into bufOut, if available.
 * @return number of bytes copied to bufOut
 */
size_t FIFO_Read(FIFO_t *f, void *bufOut, size_t nbytes);

/**
 * Allows copying a specified number of bytes from a fifo queue without
 * modifying the fifo queue.
 *
 * @param f fifo to copy data from
 * @param bufOut destination for data
 * @param nbytes number of bytes to copy into bufOut, if available.
 * @return number of bytes copied to bufOut
 */
size_t FIFO_Peek(const FIFO_t *f, void *bufOut, size_t nbytes);

#endif /* FIFO_H */
