/**
 * @file fifo.c
 */

#include <inttypes.h>
#include "fifo.h"

#define FIFO_EMPTY SIZE_MAX

static inline size_t getNextCursor(size_t cursorVal, size_t fifoSize)
{
   size_t c = cursorVal + 1;
   if (c >= fifoSize)
   {
      return 0;
   }
   else
   {
      return c;
   }
}

void FIFO_Init(FIFO_t *f, void *buf, size_t size)
{
   f->writeCursor = 0;
   f->size = size;
   f->buf = buf;
   f->readCursor = FIFO_EMPTY;
}

bool FIFO_IsEmpty(const FIFO_t *f)
{
   return (0 == FIFO_MaxReadSize(f));
}

bool FIFO_IsFull(const FIFO_t *f)
{
   return (f->readCursor == f->writeCursor);
}

size_t FIFO_MaxWriteSize(const FIFO_t *f)
{
   return f->size - FIFO_MaxReadSize(f);
}

size_t FIFO_MaxReadSize(const FIFO_t *f)
{
   if (FIFO_EMPTY == f->readCursor)
   {
      return 0;
   }
   else if (f->writeCursor > f->readCursor)
   {
      return (f->writeCursor - f->readCursor);
   }
   else
   {
      return (f->size - f->readCursor) + f->writeCursor;
   }
}

size_t FIFO_Write(FIFO_t *f, const void *bufIn, size_t nbytes)
{
   const unsigned char *p = (unsigned char *)bufIn;
   for (size_t i=0; i < nbytes; i++)
   {
      if (FIFO_IsFull(f))
      {
         return i;
      }

      f->buf[f->writeCursor] = *p++;
      if (f->readCursor == FIFO_EMPTY)
      {
         // we were empty before this write,
         // so read cursor needs to point to the byte we just wrote
         f->readCursor = f->writeCursor;
      }
      f->writeCursor = getNextCursor(f->writeCursor, f->size);
   }
   return nbytes;
}

size_t FIFO_Read(FIFO_t *f, void *bufOut, size_t nbytes)
{
   unsigned char *p = (unsigned char *)bufOut;
   for (size_t i=0; i < nbytes; i++)
   {
      if (FIFO_IsEmpty(f))
      {
         return i;
      }
      else
      {
         *p++ = f->buf[f->readCursor];
         f->readCursor = getNextCursor(f->readCursor, f->size);
         if (f->readCursor == f->writeCursor)
         {
            // we must be empty. Let's move readCursor to point out of bounds
            // to make it clear that we are empty.
            f->readCursor = FIFO_EMPTY;
         }
      }
   }
   return nbytes;
}

size_t FIFO_Peek(const FIFO_t *f, void *bufOut, size_t nbytes)
{
   unsigned char *p = (unsigned char *)bufOut;
   size_t myRead = f->readCursor;
   size_t myWrite = f->writeCursor;
   for (size_t i=0; i < nbytes; i++)
   {
      if (myRead >= f->size)
      {
         // we hit the end of the fifo
         return i;
      }
      else
      {
         *p++ = f->buf[myRead];
         myRead = getNextCursor(myRead, f->size);
         if (myRead == myWrite)
         {
            // when read hits write, we're out of data,
            // so move myRead out of the way
            myRead = FIFO_EMPTY;
         }
      }
   }
   return nbytes;
}
