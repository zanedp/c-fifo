#include <stdint.h>
#include <inttypes.h>
#include <stdbool.h>
#include <limits.h>
#include <string.h>
#include "unity.h"

#include "fifo.h"

void setUp(void)
{

}

void tearDown(void)
{

}


void test_example(void)
{
   // This code is in fifo.h's doc-comment's example
   FIFO_t myFIFO;
   uint8_t fifoBuffer[50];
   FIFO_Init(&myFIFO, fifoBuffer, sizeof(fifoBuffer));
   int writeData = 12345;
   if (FIFO_MaxWriteSize(&myFIFO) >= sizeof(writeData))
   {
      // OK to ignore return value because we verified that we
      // would have enough space by testing FIFO_MaxWriteSize()
      FIFO_Write(&myFIFO, &writeData, sizeof(writeData));
   }
   else
   {
      printf("Not enough room\n");
   }

   int readData;
   if (FIFO_Read(&myFIFO, &readData, sizeof(readData)) == sizeof(readData))
   {
      printf("Read back the value %d\n", readData);
   }
}

void test_Init(void)
{
   FIFO_t f;
   uint8_t buffer[5];
   memset(&f, 0xFF, sizeof(f));

   FIFO_Init(&f, buffer, sizeof(buffer));
   TEST_ASSERT(FIFO_IsEmpty(&f));
   TEST_ASSERT_EQUAL(0, f.writeCursor);
   TEST_ASSERT_EQUAL_PTR(buffer, f.buf);
}

#include <stdio.h>

void print_fifo(const FIFO_t *f)
{
   if (FIFO_IsEmpty(f)) {printf("[Empty]\n");}
   if (FIFO_IsFull(f)) {printf("[Full]\n");}
   printf("readCursor = %zd\n", f->readCursor);
   printf("writeCursor = %zd\n", f->writeCursor);
   for (size_t i=0; i < f->size; i++)
   {
      printf("%zu => %" PRIX8, i, f->buf[i]);
      if (i == f->readCursor) {printf(" (readCursor)");}
      if (i == f->writeCursor) {printf(" (writeCursor)");}
      printf("\n");
   }
   printf("\n");
}

void test_WriteRead_byte_matched(void)
{
   FIFO_t f;
   uint8_t buffer[3];
   memset(buffer, 0, sizeof(buffer));
   FIFO_Init(&f, buffer, sizeof(buffer));
   TEST_ASSERT(FIFO_IsEmpty(&f));
   TEST_ASSERT(!FIFO_IsFull(&f));

   uint8_t b = 1;
   uint8_t out;
   TEST_ASSERT_EQUAL(1, FIFO_Write(&f, &b, sizeof(b)));
   TEST_ASSERT(!FIFO_IsEmpty(&f));

   TEST_ASSERT_EQUAL(1, FIFO_Read(&f, &out, sizeof(out)));
   TEST_ASSERT_EQUAL(1, out);
   TEST_ASSERT(FIFO_IsEmpty(&f));

   b = 2;
   TEST_ASSERT_EQUAL(1, FIFO_Write(&f, &b, sizeof(b)));
   TEST_ASSERT_EQUAL(1, FIFO_Read(&f, &out, sizeof(out)));
   TEST_ASSERT_EQUAL(2, out);
   TEST_ASSERT(FIFO_IsEmpty(&f));

   b = 3;
   TEST_ASSERT_EQUAL(1, FIFO_Write(&f, &b, sizeof(b)));
   TEST_ASSERT_EQUAL(1, FIFO_Read(&f, &out, sizeof(out)));
   TEST_ASSERT_EQUAL(3, out);
   TEST_ASSERT(FIFO_IsEmpty(&f));

   // wraps to beginning here

   b = 4;
   TEST_ASSERT_EQUAL(1, FIFO_Write(&f, &b, sizeof(b)));
   TEST_ASSERT_EQUAL(1, FIFO_Read(&f, &out, sizeof(out)));
   TEST_ASSERT_EQUAL(4, out);
   TEST_ASSERT(FIFO_IsEmpty(&f));
}

void test_WriteRead_byte_fillThenEmpty(void)
{
   FIFO_t f;
   uint8_t buffer[3];
   memset(buffer, 0, sizeof(buffer));
   FIFO_Init(&f, buffer, sizeof(buffer));
   TEST_ASSERT(FIFO_IsEmpty(&f));
   TEST_ASSERT(!FIFO_IsFull(&f));

   uint8_t b = 1;
   TEST_ASSERT_EQUAL(1, FIFO_Write(&f, &b, sizeof(b)));
   TEST_ASSERT(!FIFO_IsEmpty(&f));
   TEST_ASSERT(!FIFO_IsFull(&f));

   b = 2;
   TEST_ASSERT_EQUAL(1, FIFO_Write(&f, &b, sizeof(b)));
   TEST_ASSERT(!FIFO_IsEmpty(&f));
   TEST_ASSERT(!FIFO_IsFull(&f));

   b = 3;
   TEST_ASSERT_EQUAL(1, FIFO_Write(&f, &b, sizeof(b)));
   TEST_ASSERT(!FIFO_IsEmpty(&f));
   TEST_ASSERT(FIFO_IsFull(&f));

   b = 4;
   TEST_ASSERT_EQUAL(0, FIFO_Write(&f, &b, sizeof(b)));
   TEST_ASSERT(!FIFO_IsEmpty(&f));
   TEST_ASSERT(FIFO_IsFull(&f));


   // full, now
   uint8_t out;
   TEST_ASSERT_EQUAL(1, FIFO_Read(&f, &out, sizeof(out)));
   TEST_ASSERT_EQUAL(1, out);
   TEST_ASSERT(!FIFO_IsEmpty(&f));
   TEST_ASSERT(!FIFO_IsFull(&f));

   TEST_ASSERT_EQUAL(1, FIFO_Read(&f, &out, sizeof(out)));
   TEST_ASSERT_EQUAL(2, out);
   TEST_ASSERT(!FIFO_IsEmpty(&f));
   TEST_ASSERT(!FIFO_IsFull(&f));

   TEST_ASSERT_EQUAL(1, FIFO_Read(&f, &out, sizeof(out)));
   TEST_ASSERT_EQUAL(3, out);
   TEST_ASSERT(FIFO_IsEmpty(&f));
   TEST_ASSERT(!FIFO_IsFull(&f));

   // got everything out
   TEST_ASSERT_EQUAL(0, FIFO_Read(&f, &out, sizeof(out)));
   TEST_ASSERT_EQUAL(3, out); // should not have changed it
   TEST_ASSERT(FIFO_IsEmpty(&f));
   TEST_ASSERT(!FIFO_IsFull(&f));
}

void test_WriteRead_byte_fillThenEmptyOffset(void)
{
   FIFO_t f;
   uint8_t buffer[3];
   memset(buffer, 0, sizeof(buffer));
   FIFO_Init(&f, buffer, sizeof(buffer));
   TEST_ASSERT(FIFO_IsEmpty(&f));
   TEST_ASSERT(!FIFO_IsFull(&f));

   uint8_t b = 7;
   TEST_ASSERT_EQUAL(1, FIFO_Write(&f, &b, sizeof(b)));
   TEST_ASSERT(!FIFO_IsEmpty(&f));
   TEST_ASSERT(!FIFO_IsFull(&f));

   // take that out so that we do the previous test "offset" from the beginning
   uint8_t out;
   TEST_ASSERT_EQUAL(1, FIFO_Read(&f, &out, sizeof(out)));
   TEST_ASSERT_EQUAL(7, out);

   b = 1;
   TEST_ASSERT_EQUAL(1, FIFO_Write(&f, &b, sizeof(b)));
   TEST_ASSERT(!FIFO_IsEmpty(&f));
   TEST_ASSERT(!FIFO_IsFull(&f));

   b = 2;
   TEST_ASSERT_EQUAL(1, FIFO_Write(&f, &b, sizeof(b)));
   TEST_ASSERT(!FIFO_IsEmpty(&f));
   TEST_ASSERT(!FIFO_IsFull(&f));

   b = 3;
   TEST_ASSERT_EQUAL(1, FIFO_Write(&f, &b, sizeof(b)));
   TEST_ASSERT(!FIFO_IsEmpty(&f));
   TEST_ASSERT(FIFO_IsFull(&f));

   b = 4;
   TEST_ASSERT_EQUAL(0, FIFO_Write(&f, &b, sizeof(b)));
   TEST_ASSERT(!FIFO_IsEmpty(&f));
   TEST_ASSERT(FIFO_IsFull(&f));


   // full, now
   TEST_ASSERT_EQUAL(1, FIFO_Read(&f, &out, sizeof(out)));
   TEST_ASSERT_EQUAL(1, out);
   TEST_ASSERT(!FIFO_IsEmpty(&f));
   TEST_ASSERT(!FIFO_IsFull(&f));

   TEST_ASSERT_EQUAL(1, FIFO_Read(&f, &out, sizeof(out)));
   TEST_ASSERT_EQUAL(2, out);
   TEST_ASSERT(!FIFO_IsEmpty(&f));
   TEST_ASSERT(!FIFO_IsFull(&f));

   TEST_ASSERT_EQUAL(1, FIFO_Read(&f, &out, sizeof(out)));
   TEST_ASSERT_EQUAL(3, out);
   TEST_ASSERT(FIFO_IsEmpty(&f));
   TEST_ASSERT(!FIFO_IsFull(&f));

   // got everything out
   TEST_ASSERT_EQUAL(0, FIFO_Read(&f, &out, sizeof(out)));
   TEST_ASSERT_EQUAL(3, out); // should not have changed it
   TEST_ASSERT(FIFO_IsEmpty(&f));
   TEST_ASSERT(!FIFO_IsFull(&f));
}

void test_WriteRead_uint16_fillThenEmptyOffset(void)
{
   FIFO_t f;
   uint8_t buffer[3*2];
   memset(buffer, 0, sizeof(buffer));
   FIFO_Init(&f, buffer, sizeof(buffer));
   TEST_ASSERT(FIFO_IsEmpty(&f));
   TEST_ASSERT(!FIFO_IsFull(&f));

   uint16_t b = 7;
   TEST_ASSERT_EQUAL(sizeof(b), FIFO_Write(&f, &b, sizeof(b)));
   TEST_ASSERT(!FIFO_IsEmpty(&f));
   TEST_ASSERT(!FIFO_IsFull(&f));

   // take that out so that we do the previous test "offset" from the beginning
   uint16_t out;
   TEST_ASSERT_EQUAL(sizeof(b), FIFO_Read(&f, &out, sizeof(out)));
   TEST_ASSERT_EQUAL(7, out);

   b = 1;
   TEST_ASSERT_EQUAL(sizeof(b), FIFO_Write(&f, &b, sizeof(b)));
   TEST_ASSERT(!FIFO_IsEmpty(&f));
   TEST_ASSERT(!FIFO_IsFull(&f));

   b = 2;
   TEST_ASSERT_EQUAL(sizeof(b), FIFO_Write(&f, &b, sizeof(b)));
   TEST_ASSERT(!FIFO_IsEmpty(&f));
   TEST_ASSERT(!FIFO_IsFull(&f));

   b = 3;
   TEST_ASSERT_EQUAL(sizeof(b), FIFO_Write(&f, &b, sizeof(b)));
   TEST_ASSERT(!FIFO_IsEmpty(&f));
   TEST_ASSERT(FIFO_IsFull(&f));

   b = 4;
   TEST_ASSERT_EQUAL(0, FIFO_Write(&f, &b, sizeof(b)));
   TEST_ASSERT(!FIFO_IsEmpty(&f));
   TEST_ASSERT(FIFO_IsFull(&f));


   // full, now
   TEST_ASSERT_EQUAL(sizeof(out), FIFO_Read(&f, &out, sizeof(out)));
   TEST_ASSERT_EQUAL(1, out);
   TEST_ASSERT(!FIFO_IsEmpty(&f));
   TEST_ASSERT(!FIFO_IsFull(&f));

   TEST_ASSERT_EQUAL(sizeof(out), FIFO_Read(&f, &out, sizeof(out)));
   TEST_ASSERT_EQUAL(2, out);
   TEST_ASSERT(!FIFO_IsEmpty(&f));
   TEST_ASSERT(!FIFO_IsFull(&f));

   TEST_ASSERT_EQUAL(sizeof(out), FIFO_Read(&f, &out, sizeof(out)));
   TEST_ASSERT_EQUAL(3, out);
   TEST_ASSERT(FIFO_IsEmpty(&f));
   TEST_ASSERT(!FIFO_IsFull(&f));

   // got everything out
   TEST_ASSERT_EQUAL(0, FIFO_Read(&f, &out, sizeof(out)));
   TEST_ASSERT_EQUAL(3, out); // should not have changed it
   TEST_ASSERT(FIFO_IsEmpty(&f));
   TEST_ASSERT(!FIFO_IsFull(&f));
}

void test_WriteRead_uint16_fillThenEmptyOffsetPeek(void)
{
   FIFO_t f;
   uint8_t buffer[3*2];
   memset(buffer, 0, sizeof(buffer));
   FIFO_Init(&f, buffer, sizeof(buffer));
   TEST_ASSERT(FIFO_IsEmpty(&f));
   TEST_ASSERT(!FIFO_IsFull(&f));

   uint16_t b = 7;
   TEST_ASSERT_EQUAL(sizeof(b), FIFO_Write(&f, &b, sizeof(b)));
   TEST_ASSERT(!FIFO_IsEmpty(&f));
   TEST_ASSERT(!FIFO_IsFull(&f));

   // take that out so that we do the previous test "offset" from the beginning
   uint16_t out;
   TEST_ASSERT_EQUAL(sizeof(b), FIFO_Peek(&f, &out, sizeof(out)));
   TEST_ASSERT_EQUAL(7, out);
   TEST_ASSERT_EQUAL(sizeof(b), FIFO_Read(&f, &out, sizeof(out)));
   TEST_ASSERT_EQUAL(7, out);

   b = 1;
   TEST_ASSERT_EQUAL(sizeof(b), FIFO_Write(&f, &b, sizeof(b)));
   TEST_ASSERT(!FIFO_IsEmpty(&f));
   TEST_ASSERT(!FIFO_IsFull(&f));

   b = 2;
   TEST_ASSERT_EQUAL(sizeof(b), FIFO_Write(&f, &b, sizeof(b)));
   TEST_ASSERT(!FIFO_IsEmpty(&f));
   TEST_ASSERT(!FIFO_IsFull(&f));

   b = 3;
   TEST_ASSERT_EQUAL(sizeof(b), FIFO_Write(&f, &b, sizeof(b)));
   TEST_ASSERT(!FIFO_IsEmpty(&f));
   TEST_ASSERT(FIFO_IsFull(&f));

   b = 4;
   TEST_ASSERT_EQUAL(0, FIFO_Write(&f, &b, sizeof(b)));
   TEST_ASSERT(!FIFO_IsEmpty(&f));
   TEST_ASSERT(FIFO_IsFull(&f));


   // full, now
   TEST_ASSERT_EQUAL(sizeof(out), FIFO_Peek(&f, &out, sizeof(out)));
   TEST_ASSERT_EQUAL(1, out);
   TEST_ASSERT_EQUAL(sizeof(out), FIFO_Read(&f, &out, sizeof(out)));
   TEST_ASSERT_EQUAL(1, out);
   TEST_ASSERT(!FIFO_IsEmpty(&f));
   TEST_ASSERT(!FIFO_IsFull(&f));

   TEST_ASSERT_EQUAL(sizeof(out), FIFO_Peek(&f, &out, sizeof(out)));
   TEST_ASSERT_EQUAL(2, out);
   TEST_ASSERT_EQUAL(sizeof(out), FIFO_Read(&f, &out, sizeof(out)));
   TEST_ASSERT_EQUAL(2, out);
   TEST_ASSERT(!FIFO_IsEmpty(&f));
   TEST_ASSERT(!FIFO_IsFull(&f));

   TEST_ASSERT_EQUAL(sizeof(out), FIFO_Peek(&f, &out, sizeof(out)));
   TEST_ASSERT_EQUAL(3, out);
   TEST_ASSERT_EQUAL(sizeof(out), FIFO_Read(&f, &out, sizeof(out)));
   TEST_ASSERT_EQUAL(3, out);
   TEST_ASSERT(FIFO_IsEmpty(&f));
   TEST_ASSERT(!FIFO_IsFull(&f));

   // got everything out
   out = 37;
   TEST_ASSERT_EQUAL(0, FIFO_Peek(&f, &out, sizeof(out)));
   TEST_ASSERT_EQUAL(37, out); // should not have changed it
   TEST_ASSERT_EQUAL(0, FIFO_Read(&f, &out, sizeof(out)));
   TEST_ASSERT_EQUAL(37, out); // should not have changed it
   TEST_ASSERT(FIFO_IsEmpty(&f));
   TEST_ASSERT(!FIFO_IsFull(&f));
}

void test_writeABigThingAndSmallThing(void)
{
   uint32_t t;
   uint32_t u;
   uint8_t buf[sizeof(t) + 1];
   FIFO_t f;
   FIFO_Init(&f, buf, sizeof(buf));
   TEST_ASSERT(FIFO_IsEmpty(&f));
   TEST_ASSERT(!FIFO_IsFull(&f));

   t = 0xaabbccdd;
   u = 0;
   TEST_ASSERT_EQUAL(sizeof(t), FIFO_Write(&f, &t, sizeof(t)));
   TEST_ASSERT(!FIFO_IsEmpty(&f));
   TEST_ASSERT_EQUAL(sizeof(u), FIFO_Read(&f, &u, sizeof(u)));
   TEST_ASSERT_EQUAL(t, u);
   TEST_ASSERT(FIFO_IsEmpty(&f));
   TEST_ASSERT(!FIFO_IsFull(&f));

   // write 0xAABBCCDD then 0x11
   TEST_ASSERT_EQUAL(sizeof(t), FIFO_Write(&f, &t, sizeof(t)));
   TEST_ASSERT(!FIFO_IsEmpty(&f));
   TEST_ASSERT(!FIFO_IsFull(&f));
   uint8_t b = 0x11;
   TEST_ASSERT_EQUAL(sizeof(b), FIFO_Write(&f, &b, sizeof(b)));
   TEST_ASSERT(!FIFO_IsEmpty(&f));
   TEST_ASSERT(FIFO_IsFull(&f));

   TEST_ASSERT_EQUAL(sizeof(u), FIFO_Peek(&f, &u, sizeof(u)));
   TEST_ASSERT_EQUAL_HEX32(t, u);
   TEST_ASSERT_EQUAL(sizeof(u), FIFO_Read(&f, &u, sizeof(u)));
   TEST_ASSERT_EQUAL_HEX32(t, u);

   uint8_t c = 0;
   TEST_ASSERT_EQUAL(sizeof(c), FIFO_Peek(&f, &c, sizeof(c)));
   TEST_ASSERT_EQUAL_HEX8(b, c);
   TEST_ASSERT_EQUAL(sizeof(c), FIFO_Read(&f, &c, sizeof(c)));
   TEST_ASSERT_EQUAL_HEX8(b, c);
}

void test_fillWithOneWrite(void)
{
   uint32_t testVal = 1234567;
   uint32_t testOut = 0;
   uint8_t buf[sizeof(testVal)];
   FIFO_t f;
   FIFO_Init(&f, buf, sizeof(buf));
   TEST_ASSERT(FIFO_IsEmpty(&f));
   TEST_ASSERT(!FIFO_IsFull(&f));

   TEST_ASSERT_EQUAL(sizeof(testVal), FIFO_Write(&f, &testVal, sizeof(testVal)));
   TEST_ASSERT(FIFO_IsFull(&f));
   TEST_ASSERT(!FIFO_IsEmpty(&f));
   TEST_ASSERT_EQUAL(sizeof(testOut), FIFO_Peek(&f, &testOut, sizeof(testOut)));
   TEST_ASSERT_EQUAL(testVal, testOut);
   testOut = -7;
   TEST_ASSERT_EQUAL(sizeof(testOut), FIFO_Read(&f, &testOut, sizeof(testOut)));
   TEST_ASSERT_EQUAL(testVal, testOut);
   TEST_ASSERT(!FIFO_IsFull(&f));
   TEST_ASSERT(FIFO_IsEmpty(&f));

   testVal = 987654;
   TEST_ASSERT_EQUAL(sizeof(testVal), FIFO_Write(&f, &testVal, sizeof(testVal)));
   TEST_ASSERT(FIFO_IsFull(&f));
   TEST_ASSERT(!FIFO_IsEmpty(&f));
   TEST_ASSERT_EQUAL(sizeof(testOut), FIFO_Read(&f, &testOut, sizeof(testOut)));
   TEST_ASSERT_EQUAL(testVal, testOut);
   TEST_ASSERT(!FIFO_IsFull(&f));
   TEST_ASSERT(FIFO_IsEmpty(&f));
}

void test_SizeTest(void)
{
   uint8_t buffer[5];
   FIFO_t f;
   FIFO_Init(&f, buffer, sizeof(buffer));

   TEST_ASSERT(FIFO_IsEmpty(&f));
   TEST_ASSERT_EQUAL(0, FIFO_MaxReadSize(&f)); // empty is a special case
   TEST_ASSERT_EQUAL(sizeof(buffer), FIFO_MaxWriteSize(&f));

   // write > read cursor
   uint8_t b = 7;
   uint32_t i = 0x11223344;
   TEST_ASSERT_EQUAL(1, FIFO_Write(&f, &b, 1));
   TEST_ASSERT(!FIFO_IsEmpty(&f));
   TEST_ASSERT(!FIFO_IsFull(&f));
   TEST_ASSERT_EQUAL(1, FIFO_MaxReadSize(&f));
   TEST_ASSERT_EQUAL(sizeof(buffer) - 1, FIFO_MaxWriteSize(&f));

   // write == read cursor
   TEST_ASSERT_EQUAL(sizeof(i), FIFO_Write(&f, &i, sizeof(i)));
   TEST_ASSERT(!FIFO_IsEmpty(&f));
   TEST_ASSERT(FIFO_IsFull(&f));
   TEST_ASSERT_EQUAL(sizeof(i) + sizeof(b), FIFO_MaxReadSize(&f));
   TEST_ASSERT_EQUAL(sizeof(buffer) - sizeof(i) - sizeof(b), FIFO_MaxWriteSize(&f));

   // write == read, != 0
   TEST_ASSERT_EQUAL(sizeof(b), FIFO_Read(&f, &b, sizeof(b)));
   TEST_ASSERT_EQUAL(sizeof(b), FIFO_Write(&f, &b, sizeof(b)));
   TEST_ASSERT(!FIFO_IsEmpty(&f));
   TEST_ASSERT(FIFO_IsFull(&f));
   TEST_ASSERT_EQUAL(sizeof(i) + sizeof(b), FIFO_MaxReadSize(&f));
   TEST_ASSERT_EQUAL(sizeof(buffer) - (sizeof(i) + sizeof(b)), FIFO_MaxWriteSize(&f));

   // read > write, write == 0
   TEST_ASSERT_EQUAL(sizeof(b), FIFO_Read(&f, &b, sizeof(b)));
   TEST_ASSERT(!FIFO_IsEmpty(&f));
   TEST_ASSERT(!FIFO_IsFull(&f));
   TEST_ASSERT_EQUAL(sizeof(i), FIFO_MaxReadSize(&f));
   TEST_ASSERT_EQUAL(sizeof(buffer) - sizeof(i), FIFO_MaxWriteSize(&f));

   TEST_ASSERT_EQUAL(sizeof(b), FIFO_Read(&f, &b, sizeof(b)));
   TEST_ASSERT(!FIFO_IsEmpty(&f));
   TEST_ASSERT(!FIFO_IsFull(&f));
   TEST_ASSERT_EQUAL(sizeof(i)-sizeof(b), FIFO_MaxReadSize(&f));
   TEST_ASSERT_EQUAL(sizeof(buffer) - (sizeof(i)-sizeof(b)), FIFO_MaxWriteSize(&f));

   TEST_ASSERT_EQUAL(sizeof(b), FIFO_Read(&f, &b, sizeof(b)));
   TEST_ASSERT(!FIFO_IsEmpty(&f));
   TEST_ASSERT(!FIFO_IsFull(&f));
   TEST_ASSERT_EQUAL(sizeof(i)-sizeof(b)-sizeof(b), FIFO_MaxReadSize(&f));
   TEST_ASSERT_EQUAL(sizeof(buffer) - (sizeof(i)-sizeof(b)-sizeof(b)), FIFO_MaxWriteSize(&f));

   // read > write, and write != 0
   b = 0x99;
   TEST_ASSERT_EQUAL(sizeof(b), FIFO_Write(&f, &b, sizeof(b)));
   TEST_ASSERT(!FIFO_IsEmpty(&f));
   TEST_ASSERT(!FIFO_IsFull(&f));
   TEST_ASSERT_EQUAL(sizeof(i)-sizeof(b)-sizeof(b)+sizeof(b), FIFO_MaxReadSize(&f));
   TEST_ASSERT_EQUAL(sizeof(buffer) - (sizeof(i)-sizeof(b)-sizeof(b)+sizeof(b)), FIFO_MaxWriteSize(&f));

}
